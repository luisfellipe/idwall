import React from "react";
import styled from "styled-components";

const FaceMatchBody = styled.div`
  background: white;
  display: flex;
  flex-direction: column;
  width: 100%;
  padding-bottom: 60px;
  padding-top: 30px;
`;

const FaceMatchTitle = styled.div`
  color: rgb(99, 0, 255);
  font-size: 42px;
  font-family: "Caviar Dreams", sans-serif;
  text-align: center;
  font-weight: 800;
  @media (max-width: 768px) {
    font-size: 24px;
  }
`;

const FaceMatchInfo = styled.div`
  color: black;
  font-size: 24px;
  font-family: "Lato", sans-serif;
  text-align: center;
  padding-top: 15px;
  align-items: center;
  justify-content: center;
  padding: 45px;
  @media (max-width: 768px) {
    font-size: 18px;
  }
`;

const FaceMatchPontos = styled.div`
  display: flex;
  display: column;
  font-size: 18px;
  text-transform: uppercase;
  font-family: "Lato", sans-serif;
  padding-top: 30px;
  justify-content: center;
  align-items: center;
  color: black;
  line-height: 200%;
  @media (max-width: 768px) {
    font-size: 16px;
  }
  @media (max-width: 425px) {
    font-size: 14px;
  }
`;

const Vl = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-left: 1px solid rgb(99, 0, 255);
  height: 220px;
  padding: 15px;
  margin-left: 27px;
  @media (max-width: 768px) {
    height: 250px;
  }
  @media (max-width: 425px) {
    height: 300px;
  }
`;

const FaceMatch = () => {
  return (
    <FaceMatchBody>
      <FaceMatchTitle>Face Match</FaceMatchTitle>
      <FaceMatchInfo>
        <b>
          Nossa tecnologia de reconhecimento facial reduz o risco de fraudes por
          personificação.
        </b>{" "}
        Por meio da comparação de uma foto tirada no momento do cadastro e da
        foto usada no documento, nós confirmamos que seus usuários são quem
        realmente dizem ser.
      </FaceMatchInfo>
      <FaceMatchPontos>
        <Vl />
        REDUZA FRAUDES DE IDENTIDADE
        <br />
        AUTOMATIZE SEU BACKOFFICE
        <br />
        TOTALMENTE INTEGRADO AOS OUTROS PRODUTOS DA IDWALL BIOMETRIA FACIAL
        FEITA COM INTELIGÊNCIA ARTIFICIAL
        <br />
        FACE LIVENESS: PROVA DE VIDA COM SORRISO PRESENTE NO SDK
        <br />
      </FaceMatchPontos>
    </FaceMatchBody>
  );
};

export default FaceMatch;
