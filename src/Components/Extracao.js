import React from "react";
import styled from "styled-components";

const ExtracaoBody = styled.div`
  background: rgb(99, 0, 255);
  background: linear-gradient(
    180deg,
    rgba(99, 0, 255, 1) 0%,
    rgba(99, 0, 255, 0.6) 100%
  );
  display: flex;
  flex-direction: column;
  width: 100%;
  padding-bottom: 60px;
  padding-top: 30px;
`;

const ExtracaoTitle = styled.div`
  display: flex;
  font-family: "Caviar Dreams", sans-serif;
  width: 100%;
  font-size: 42px;
  color: white;
  text-align: center;
  justify-content: center;
  font-weight: 800;
  @media (max-width: 768px) {
    font-size: 24px;
  }
`;

const ExtracaoInfo = styled.div`
  color: white;
  font-size: 24px;
  font-family: "Lato", sans-serif;
  text-align: center;
  padding-top: 15px;
  align-items: center;
  justify-content: center;
  padding: 45px;
  @media (max-width: 768px) {
    font-size: 18px;
  }
`;

const ExtracaoPontos = styled.div`
  display: flex;
  display: column;
  font-size: 18px;
  text-transform: uppercase;
  font-family: "Lato", sans-serif;
  padding-top: 30px;
  justify-content: center;
  align-items: center;
  color: white;
  line-height: 200%;
  @media (max-width: 768px) {
    font-size: 16px;
  }
  @media (max-width: 425px) {
    font-size: 14px;
  }
`;

const Vl = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-left: 1px solid white;
  height: 220px;
  padding: 15px;
  margin-left: 27px;
  @media (max-width: 768px) {
    height: 250px;
  }
  @media (max-width: 425px) {
    height: 300px;
  }
  @media (max-width: 320px) {
    height: 420px;
  }
`;

const Extracao = () => {
  return (
    <ExtracaoBody>
      <ExtracaoTitle>Extração de dados de documentos com OCR</ExtracaoTitle>
      <ExtracaoInfo>
        Nossa solução recebe a foto do documento, passa essa imagem por um
        processo de machine learning (OCR) e extrai automaticamente todas as
        informações. Você pode verificar a integridade e a consistência dos
        dados aliando essa solu- ção ao nosso Background Check.
      </ExtracaoInfo>
      <ExtracaoPontos>
        <Vl /> REDUZA SEU BACKOFFICE MANUAL <br /> VALIDE A IDENTIDADE E O RISCO
        DE SEU CLIENTE <br /> UTILIZAMOS MACHINE LEARNING PARA O RECONHECIMENTO
        ÓPTICO DE CARACTERES <br /> FOQUE EM DADOS CRUCIAIS NA EXTRAÇÃO E
        OTIMIZE AINDA MAIS SUA OPERAÇÃO <br /> MODELO DE CONFIANÇA PERMITE UMA
        MELHOR ANÁLISE AUTOMÁTICA DE TIPIFICAÇÃO <br /> RECUSA AUTOMÁTICA DE
        FOTOS QUE NÃO SEJAM DE DOCUMENTOS <br /> TIPIFICAÇÃO AUTOMÁTICA
      </ExtracaoPontos>
    </ExtracaoBody>
  );
};

export default Extracao;
