import React from "react";
import styled from "styled-components";
import Extracao from "./Extracao";
import Fazemos from "./Fazemos";
import FaceMatch from "./FaceMatch";
import BackgroundCheck from "./BackgroundCheck";

const All = styled.div`
  width: 100%;
  height: 100%;
  background: rgb(99, 0, 255);
  background: linear-gradient(
    180deg,
    rgba(99, 0, 255, 1) 0%,
    rgba(99, 0, 255, 0.6) 100%
  );
  overflow: hidden;
`;

const Header = styled.div`
  color: white;
`;

const Title = styled.div`
  display: flex;
  font-family: "Caviar Dreams", sans-serif;
  width: 100%;
  font-size: 56px;
  color: white;
  text-transform: uppercase;
  padding: 15px;
  text-align: center;
  justify-content: center;
  @media (max-width: 768px) {
    font-size: 36px;
  }
`;

const Body = styled.div``;

const QuemSomos = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  padding-top: 60px;
  padding-bottom: 60px;
  color: white;
`;

const QuemTitulo = styled.div`
  font-family: "Caviar Dreams", sans-serif;
  font-size: 42px;
  font-weight: 800;
  padding: 45px;
  text-align: center;
  @media (max-width: 768px) {
    font-size: 24px;
  }
`;

const QuemTexto = styled.div`
  font-family: "Lato", sans-serif;
  padding: 15px;
  font-size: 24px;
  padding: 45px;
  @media (max-width: 768px) {
    font-size: 18px;
  }
`;

const Home = () => {
  return (
    <All>
      <Header>
        <Title>idwall</Title>
      </Header>
      <Body>
        <QuemSomos>
          <QuemTitulo>
            Somos a maior referência do mercado em onboarding e identidade.
          </QuemTitulo>
          <QuemTexto>
            A idwall fornece{" "}
            <b>
              soluções de combate à fraude de identidade, compliance e gestão de
              risco
            </b>{" "}
            integradas em um só lugar para negócios continuarem escalando com
            segurança, sem comprometer a experiência do usuário.
            <br />
            <br />
            Para nós,{" "}
            <b>
              o processo de onboarding é mais do que uma burocracia necessária,
              é uma vantagem competitiva
            </b>{" "}
            capaz de aumentar a retenção de usuários e garantir toda a segurança
            e compliance do seu negócio.
          </QuemTexto>
        </QuemSomos>
        <Fazemos />
        <Extracao />
        <FaceMatch />
        <BackgroundCheck />
      </Body>
    </All>
  );
};

export default Home;
