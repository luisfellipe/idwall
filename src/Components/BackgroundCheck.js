import React from "react";
import styled from "styled-components";

const BackgroundBody = styled.div`
  background: rgb(99, 0, 255);
  background: linear-gradient(
    180deg,
    rgba(99, 0, 255, 1) 0%,
    rgba(99, 0, 255, 0.6) 100%
  );
  display: flex;
  flex-direction: column;
  width: 100%;
  padding-bottom: 60px;
  padding-top: 30px;
`;

const BackgroundTitle = styled.div`
  color: white;
  font-size: 42px;
  font-family: "Caviar Dreams", sans-serif;
  text-align: center;
  font-weight: 800;
  @media (max-width: 768px) {
    font-size: 24px;
  }
`;

const BackgroundInfo = styled.div`
  color: white;
  font-size: 24px;
  font-family: "Lato", sans-serif;
  text-align: center;
  padding-top: 15px;
  align-items: center;
  justify-content: center;
  padding: 45px;
  @media (max-width: 768px) {
    font-size: 18px;
  }
`;

const BackgroundPontos1 = styled.div`
  display: flex;
  display: column;
  font-size: 18px;
  text-transform: uppercase;
  font-family: "Lato", sans-serif;
  padding-top: 30px;
  justify-content: center;
  align-items: center;
  color: white;
  line-height: 200%;
  width: 50%;
  @media (max-width: 768px) {
    font-size: 16px;
  }
  @media (max-width: 425px) {
    font-size: 14px;
    width: 100%;
    margin: 15px;
  }
`;

const BgPontos = styled.div`
  display: flex;
  flex-direction: row;
  @media (max-width: 768px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

const BackgroundPontos2 = styled.div`
  display: flex;
  display: column;
  font-size: 18px;
  text-transform: uppercase;
  font-family: "Lato", sans-serif;
  padding-top: 30px;
  justify-content: center;
  align-items: center;
  color: white;
  line-height: 200%;
  width: 50%;
  @media (max-width: 768px) {
    font-size: 16px;
  }
  @media (max-width: 425px) {
    font-size: 14px;
    width: 100%;
    margin: 15px;
  }
`;

const Vl = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-left: 1px solid white;
  height: 280px;
  padding: 15px;
  margin-left: 27px;
  @media (max-width: 768px) {
    height: 350px;
  }
  @media (max-width: 425px) {
    height: 380px;
  }
`;
const CTA = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 100px;
`;
const Button = styled.button`
  display: inline-block;
  color: black;
  background: white;
  font-size: 1.2em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid #6300ff;
  border-radius: 15px;
  display: block;
  width: 350px;
  height: 50px;
  font-family: "Lato", sans-serif;
  text-transform: uppercase;
  @media (max-width: 320px) {
    width: 260px;
    font-size: 1em;
  }
`;

const BackgroundCheck = () => {
  return (
    <BackgroundBody>
      <BackgroundTitle>Background Check</BackgroundTitle>
      <BackgroundInfo>
        Faça consulta de dados de forma ágil e prática com a nossa solução de
        Background Check.
        <b>
          Consulte automaticamente informações essenciais em mais de 250 fontes
          de dados
        </b>
        &nbsp;e agilize o processo de onboarding digital de clientes e
        parceiros, sem deixar de lado a segu- rança do seu negócio.
      </BackgroundInfo>
      <BgPontos>
        <BackgroundPontos1>
          <Vl />
          SOLUÇÃO ALTAMENTE PERSONALIZÁVEL <br />
          MAIS TECNOLOGIA PARA SEU COMPLIANCE
          <br />
          ATENDA KYC & PLD SEM FRICÇÃO PARA SEUS CLIENTES <br />
          GESTÃO DE RISCO INTELIGENTE E PRÁTICA <br />
          BUSCAS AUTOMATIZADAS NAS FONTES DE DADOS DADOS
          <br />
          MAIS ATUALIZADOS SIGNIFICAM MAIS SEGURANÇA <br />
          INTEGRAÇÃO COM BIRÔS DE CRÉDITO SE VOCÊ JÁ TIVER CONTA
        </BackgroundPontos1>
        <BackgroundPontos2>
          <Vl />
          PROTEJA SUA OPERAÇÃO DE FRAUDES DE IDENTIDADE <br />
          CONSULTAS PARA PF E PJ
          <br />
          FLUXO DE APROVAÇÃO DE ACORDO COM AS SUAS REGRAS DE NEGÓCIOS
          <br />
          PROCESSO DE VERIFICAÇÃO EM FASES, OU SEJA, ALGUMAS BUSCAS SÃO
          ACIONADAS APENAS EM DETERMINADAS CONDIÇÕES
          <br />
          FILTRE A CONSULTA DE PROCESSOS POR TIPO DE CRIME <br />
          VEJA NOSSO CATÁLOGO DE CONSULTAS
        </BackgroundPontos2>
      </BgPontos>
      <CTA>
        <a href="https://idwall.co/contato/">
          <Button>Entre em contato conosco </Button>
        </a>
      </CTA>
    </BackgroundBody>
  );
};

export default BackgroundCheck;
