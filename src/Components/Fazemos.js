import React, { useState } from "react";
import { Carousel } from "antd";
import styled from "styled-components";
import "antd/dist/antd.css";
import img1 from "../Imagens/1.png";
import img2 from "../Imagens/2.png";
import img3 from "../Imagens/3.png";

const FazemosBody = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 30px;
  width: 100%;
  background: white;
`;

const FazemosTitulo = styled.div`
  color: rgb(99, 0, 255);
  font-size: 42px;
  font-family: "Caviar Dreams", sans-serif;
  text-align: center;
  font-weight: 800;
  padding: 45px;
  @media (max-width: 768px) {
    font-size: 24px;
  }
`;

const FazemosTexto = styled.div`
  color: black;
  font-size: 24px;
  font-family: "Lato", sans-serif;
  text-align: center;
  padding: 45px;
  align-items: center;
  justify-content: center;
  @media (max-width: 768px) {
    font-size: 18px;
  }
`;

const FazemosImg = styled.div`
  display: flex;
  flex-direction: row;
  padding: 10px;
  align-items: center;
  justify-content: center;
`;

const ImgTodas = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 15px;
  padding-bottom: 60px;
  padding-right: 15px;
  padding-left: 15px;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const Img1 = styled.img`
  max-width: 100%;
  height: auto;
  @media (max-width: 768px) {
    padding-bottom: 25px;
  }
`;

const Img2 = styled.img`
  max-width: 100%;
  height: auto;
  @media (max-width: 768px) {
    padding-bottom: 25px;
  }
`;

const Img3 = styled.img`
  max-width: 100%;
  height: auto;
  @media (max-width: 768px) {
    padding-bottom: 25px;
  }
`;

const ImgTexto = styled.div`
  text-transform: uppercase;
  font-family: "Lato", sans-serif;
  font-size: 20px;
  padding-left: 10px;
  color: black;
  @media (max-width: 768px) {
    font-size: 18px;
  }
  @media (max-width: 425px) {
    font-size: 16px;
    text-align: center;
  }
`;

const Obs = styled.div`
  display: flex;
  text-transform: uppercase;
  font-family: "Lato", sans-serif;
  font-size: 24px;
  color: black;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding-bottom: 60px;
  padding-left: 45px;
  padding-right: 45px;
  @media (max-width: 768px) {
    font-size: 18px;
  }
`;

const Fazemos = () => {
  return (
    <FazemosBody>
      <FazemosTitulo>Como funciona a nossa solução completa</FazemosTitulo>
      <FazemosTexto>
        Ao integrar a nossa solução no seu aplicativo, o usuário primeiro tira
        uma foto do documento e, depois, uma selfie com prova de vida, na qual
        ele sorri no final e está validado.
      </FazemosTexto>
      <FazemosImg>
        <ImgTodas>
          <Img1 alt="" src={img1} />
          <ImgTexto>O usuário tira uma foto do documento.</ImgTexto>
          <Img2 alt="" src={img2} />
          <ImgTexto>
            Depois, o usuário tira uma selfie sorrindo, que serve como uma prova
            de vida
          </ImgTexto>
          <Img3 alt="" src={img3} />
          <ImgTexto>
            E pronto! Nós fazemos com que o cadastro seja fácil e seguro.
          </ImgTexto>
        </ImgTodas>
      </FazemosImg>
      <Obs>
        Você também pode usar a solução da idwall separadamente em cada etapa do
        seu fluxo de verificação
      </Obs>
    </FazemosBody>
  );
};

export default Fazemos;
